# Raytracing

This repository holds course notes from the Pixar Raytracing course based on the work of Peter Shirley in Raytracing in One Weekend. The course uses ShaderToy to process OpenGL shaders, but this repository also holds work per chapter of the following Peter Shirley works:

- Raytracing in One Weekend
- Raytracing The Next Week
- Ray Tracing The Rest of Your Life

## Compiling and Running Code:

Dependencies:

* `make`
* C++ compiler: `clang++` [default], `g++`.

The Makefiles have a few basic targets:

* all: Will build imager
* imager: Will build the imager binary
* clean: Will remove the imager binary and render.ppm image
* render: Run imager target and render the render.ppm image

By default the Makefile will use `clang++`, but this can be overwritten by specifying the CXX compiler of choice: `make CXX=g++ <target>`.
